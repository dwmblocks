static const Block blocks[] = {
	// <Icon> <Command> <Update> <Interval> <Update> <Signal>
	{"[", "cat /sys/class/power_supply/BAT0/capacity", 5, 0},
	{"%, ", "awk '{print tolower($0)}' /sys/class/power_supply/BAT0/status", 5, 9},
	{"] [", "amixer sget Master | awk -F'[][]' '/dB/ {print $2 \", \" $6}'", 5, 10},
	{"] [", "date '+%A %Y-%m-%d %I:%M %p]'", 5, 0},
};
